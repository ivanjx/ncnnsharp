﻿using ncnnsharp;
using System;
using System.IO;

namespace ncnnsharp.Test
{
    class Program
    {
        static readonly string[] classes = new[]
        {"background", "person", "bicycle",
        "car", "motorbike", "aeroplane", "bus", "train", "truck",
        "boat", "traffic light", "fire hydrant", "stop sign",
        "parking meter", "bench", "bird", "cat", "dog", "horse",
        "sheep", "cow", "elephant", "bear", "zebra", "giraffe",
        "backpack", "umbrella", "handbag", "tie", "suitcase",
        "frisbee", "skis", "snowboard", "sports ball", "kite",
        "baseball bat", "baseball glove", "skateboard", "surfboard",
        "tennis racket", "bottle", "wine glass", "cup", "fork",
        "knife", "spoon", "bowl", "banana", "apple", "sandwich",
        "orange", "broccoli", "carrot", "hot dog", "pizza", "donut",
        "cake", "chair", "sofa", "pottedplant", "bed", "diningtable",
        "toilet", "tvmonitor", "laptop", "mouse", "remote", "keyboard",
        "cell phone", "microwave", "oven", "toaster", "sink",
        "refrigerator", "book", "clock", "vase", "scissors",
        "teddy bear", "hair drier", "toothbrush"
        };

        static void Main(string[] args)
        {
            // YOLOv4.
            Console.Write("Input-> ");
            string inpath = Console.ReadLine().Replace("\"", string.Empty);
            Console.Write("Model-> ");
            string modelpath = Console.ReadLine().Replace("\"", string.Empty);
            Console.Write("Param-> ");
            string parampath = Console.ReadLine().Replace("\"", string.Empty);
            Console.Write("Output-> ");
            string outpath = Console.ReadLine().Replace("\"", string.Empty);

            // Load image.
            CvMat inimg = CvMat.FromFile(inpath);

            // Init yolo.
            using (Net yolo = new Net())
            using (Option opt = new Option())
            {
                opt.NumThreads = 4;
                opt.UseWinogradConvolution = true;
                opt.UseSgemmConvolution = true;
                opt.UseFP16Packed = true;
                opt.UseFP16Storage = true;
                opt.UseFP16Arithmetic = true;
                opt.UsePackingLayout = true;
                opt.UseShaderPack8 = false;
                opt.UseImageStorage = false;

                yolo.SetOption(opt);
                yolo.LoadParam(parampath);
                yolo.LoadModel(modelpath);

                // Init mat.
                Mat inmat = Mat.FromPixelsResize(
                    inimg.Data,
                    65538,
                    inimg.Cols,
                    inimg.Rows,
                    608,
                    608);
                using (inmat)
                {
                    float[] meanVals = new[] { 0f, 0f, 0f };
                    float[] normVals = new[] { 1 / 255f, 1 / 255f, 1 / 255f };
                    inmat.SubstractMeanNormalize(meanVals, normVals);

                    // Init extractor.
                    using (Extractor extractor = yolo.CreateExtractor())
                    {
                        extractor.Input("data", inmat);

                        // Getting output.
                        using (Mat outmat = extractor.Extract("output"))
                        using (CvMat outimg = inimg.Clone())
                        {
                            for (int i = 0; i < outmat.H; ++i)
                            {
                                float[] result = outmat.GetRow(i);
                                string label = classes[(int)result[0]];
                                float prob = result[1];
                                float x = result[2] * inimg.Cols;
                                float y = result[3] * inimg.Rows;
                                float width = result[4] * inimg.Cols - x;
                                float height = result[5] * inimg.Rows - y;

                                Console.WriteLine("{0}. {1} [{2}%] at {3}:{4}",
                                    i + 1,
                                    label,
                                    prob * 100,
                                    x,
                                    y);

                                outimg.DrawRectangle(
                                    new CvRect()
                                    {
                                        X = (uint)x,
                                        Y = (uint)y,
                                        Width = (uint)width,
                                        Height = (uint)height
                                    },
                                    new CvColor()
                                    {
                                        R = 0,
                                        G = 255,
                                        B = 0,
                                        A = 1
                                    },
                                    2);
                                string txt = string.Format("{0} [{1:F1}%]", label, prob * 100);
                                outimg.DrawText(
                                    txt,
                                    new CvPoint()
                                    {
                                        X = (uint)x,
                                        Y = (uint)y
                                    },
                                    CvFont.FONT_HERSHEY_PLAIN,
                                    0.3,
                                    new CvColor()
                                    {
                                        R = 0,
                                        G = 0,
                                        B = 255,
                                        A = 1
                                    },
                                    1);

                                outimg.Write(outpath);
                            }
                        }
                    }
                }
            }

            Console.WriteLine("Done");
        }
    }
}
