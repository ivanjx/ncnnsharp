﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ncnnsharp
{
    [StructLayout(LayoutKind.Sequential)]
    public struct CvPoint
    {
        public uint X;
        public uint Y;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct CvRect
    {
        public uint X;
        public uint Y;
        public uint Width;
        public uint Height;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct CvColor
    {
        public byte R;
        public byte G;
        public byte B;
        public byte A;
    }

    // https://codeyarns.com/tech/2015-03-11-fonts-in-opencv.html
    public enum CvFont
    {
        FONT_HERSHEY_SIMPLEX = 0, //!< normal size sans-serif font
        FONT_HERSHEY_PLAIN = 1, //!< small size sans-serif font
        FONT_HERSHEY_DUPLEX = 2, //!< normal size sans-serif font (more complex than FONT_HERSHEY_SIMPLEX)
        FONT_HERSHEY_COMPLEX = 3, //!< normal size serif font
        FONT_HERSHEY_TRIPLEX = 4, //!< normal size serif font (more complex than FONT_HERSHEY_COMPLEX)
        FONT_HERSHEY_COMPLEX_SMALL = 5, //!< smaller version of FONT_HERSHEY_COMPLEX
        FONT_HERSHEY_SCRIPT_SIMPLEX = 6, //!< hand-writing style font
        FONT_HERSHEY_SCRIPT_COMPLEX = 7, //!< more complex variant of FONT_HERSHEY_SCRIPT_SIMPLEX
        FONT_ITALIC = 16 //!< flag for italic font
    }

    public class CvMat : IDisposable
    {
        IntPtr m_handle;

        public bool IsEmpty
        {
            get
            {
                int val = Native.ncnn_cv_mat_get_empty(m_handle);
                return val != 0;
            }
        }

        public int Channels
        {
            get
            {
                return Native.ncnn_cv_mat_get_channels(m_handle);
            }
        }

        public int Type
        {
            get
            {
                return Native.ncnn_cv_mat_get_type(m_handle);
            }
        }

        public IntPtr Data
        {
            get
            {
                return Native.ncnn_cv_mat_get_data(m_handle);
            }
        }

        public int Rows
        {
            get
            {
                return Native.ncnn_cv_mat_get_rows(m_handle);
            }
        }

        public int Cols
        {
            get
            {
                return Native.ncnn_cv_mat_get_cols(m_handle);
            }
        }

        public CvMat(IntPtr handle)
        {
            m_handle = handle;
        }

        public CvMat Clone()
        {
            return new CvMat(
                Native.ncnn_cv_mat_clone(m_handle));
        }

        public CvMat Resize(
            CvPoint size, 
            float sw = 0, 
            float sh = 0, 
            int flags = 0)
        {
            return new CvMat(
                Native.ncnn_cv_resize(
                    m_handle, 
                    size, 
                    sw, 
                    sh, 
                    flags));
        }

        public void DrawRectangle(CvRect rect, CvColor color, int thickness)
        {
            Native.ncnn_cv_mat_rectangle(m_handle, rect, color, thickness);
        }

        public void DrawCircle(CvPoint center, int radius, CvColor color, int thickness)
        {
            Native.ncnn_cv_mat_circle(m_handle, center, radius, color, thickness);
        }

        public void DrawLine(CvPoint pt1, CvPoint pt2, CvColor color, int thickness)
        {
            Native.ncnn_cv_mat_line(m_handle, pt1, pt2, color, thickness);
        }

        public void DrawText(
            string text, 
            CvPoint pt, 
            CvFont fontFace, 
            double fontScale, 
            CvColor color, 
            int thickness)
        {
            Native.ncnn_cv_mat_text(
                m_handle,
                text,
                pt,
                (int)fontFace,
                fontScale,
                color,
                thickness);
        }

        public void Write(string path)
        {
            Native.ncnn_cv_imwrite(m_handle, path);
        }

        public void Write(Stream resultStream)
        {
            // For now there is no direct API to save to buffer in any format other than png.
            // Follow up: https://github.com/Tencent/ncnn/issues/2988

            // Getting native memory buffer and its length.
            int len;
            IntPtr ptr = Native.ncnn_cv_imwrite_mem(m_handle, out len);

            if (ptr == IntPtr.Zero)
            {
                throw new Exception("Failed to write to buffer");
            }

            try
            {
                // Converting to byte array.
                byte[] buff = new byte[len];
                Marshal.Copy(ptr, buff, 0, len);

                // Write.
                resultStream.Write(buff, 0, len);
            }
            finally
            {
                // Destroy native memory buffer.
                Native.ncnn_cv_imwrite_mem_destroy(ptr);
            }
        }

        public void Dispose()
        {
            Native.ncnn_cv_mat_destroy(m_handle);
        }

        /// <summary>
        /// IMREAD_UNCHANGED = -1,
        /// IMREAD_GRAYSCALE = 0,
        /// IMREAD_COLOR = 1
        /// </summary>
        /// <param name="path"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        public static CvMat FromFile(string path, int flags = 1)
        {
            return new CvMat(
                Native.ncnn_cv_imread(path, flags));
        }

        public static CvMat FromBuffer(byte[] buffer, int len, int flags = 1)
        {
            return new CvMat(
                Native.ncnn_cv_imread_mem(buffer, len, flags));
        }
    }
}
