﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ncnnsharp
{
    public class Net : IDisposable
    {
        IntPtr m_handle;

        public Net()
        {
            m_handle = Native.ncnn_net_create();
        }

        public void SetOption(Option opt)
        {
            Native.ncnn_net_set_option(m_handle, opt.Handle);
        }

        public void LoadParam(string path)
        {
            Native.ncnn_net_load_param(m_handle, path);
        }

        public void LoadParam(byte[] buffer)
        {
            Native.ncnn_net_load_param_memory(m_handle, buffer);
        }

        public void LoadParamBin(string path)
        {
            Native.ncnn_net_load_param_bin(m_handle, path);
        }

        public void LoadParamBin(byte[] buffer)
        {
            Native.ncnn_net_load_param_bin_memory(m_handle, buffer);
        }

        public void LoadModel(string path)
        {
            Native.ncnn_net_load_model(m_handle, path);
        }

        public void LoadModel(byte[] buffer)
        {
            Native.ncnn_net_load_model_memory(m_handle, buffer);
        }

        public void Clear()
        {
            Native.ncnn_net_clear(m_handle);
        }

        public Extractor CreateExtractor()
        {
            return new Extractor(
                Native.ncnn_extractor_create(m_handle));
        }

        public void Dispose()
        {
            Native.ncnn_net_destroy(m_handle);
        }
    }
}
