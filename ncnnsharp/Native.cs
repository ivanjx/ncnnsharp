using ncnnsharp;
using System;
using System.Runtime.InteropServices;

namespace ncnnsharp
{
    internal static class Native
    {
        const string LIB = "ncnn"; // libncnn.so
        const CallingConvention CONV = CallingConvention.Cdecl;

        #region OPTION_API

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_option_create();

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_option_destroy(IntPtr option);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_option_get_num_threads(IntPtr option);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_option_set_num_threads(IntPtr option, int num_threads);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_option_get_use_vulkan_compute(IntPtr option);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_option_set_use_vulkan_compute(IntPtr option, int use_vulkan_compute);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_option_get_use_winograd_convolution(IntPtr option);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_option_set_use_winograd_convolution(IntPtr option, int use_winograd_convolution);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_option_get_use_sgemm_convolution(IntPtr option);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_option_set_use_sgemm_convolution(IntPtr option, int use_sgemm_convolution);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_option_get_use_fp16_storage(IntPtr option);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_option_set_use_fp16_storage(IntPtr option, int use_fp16_storage);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_option_get_use_fp16_packed(IntPtr option);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_option_set_use_fp16_packed(IntPtr option, int use_fp16_packed);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_option_get_use_fp16_arithmetic(IntPtr option);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_option_set_use_fp16_arithmetic(IntPtr option, int use_fp16_arithmetic);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_option_get_use_packing_layout(IntPtr option);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_option_set_use_packing_layout(IntPtr option, int use_packing_layout);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_option_get_use_shader_pack8(IntPtr option);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_option_set_use_shader_pack8(IntPtr option, int use_shader_pack8);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_option_get_use_image_storage(IntPtr option);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_option_set_use_image_storage(IntPtr option, int use_image_storage);

        #endregion

        #region ALLOCATOR_API

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_allocator_create_pool_allocator();

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_allocator_create_unlocked_pool_allocator();

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_allocator_destroy(IntPtr allocator);

        #endregion

        #region MAT_API

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_create_1d(int w, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_create_2d(int w, int h, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_create_3d(int w, int h, int c, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_create_external_1d(int w, IntPtr data, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_create_external_2d(int w, int h, IntPtr data, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_create_external_3d(int w, int h, int c, IntPtr data, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_create_1d_elem(int w, ulong elemsize, int elempack, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_create_2d_elem(int w, int h, ulong elemsize, int elempack, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_create_3d_elem(int w, int h, int c, ulong elemsize, int elempack, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_create_external_1d_elem(int w, IntPtr data, ulong elemsize, int elempack, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_create_external_2d_elem(int w, int h, IntPtr data, ulong elemsize, int elempack, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_create_external_3d_elem(int w, int h, int c, IntPtr data, ulong elemsize, int elempack, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_mat_destroy(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_mat_fill_float(IntPtr mat, float v);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_clone(IntPtr mat, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_reshape_1d(IntPtr mat, int w, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_reshape_2d(IntPtr mat, int w, int h, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_reshape_3d(IntPtr mat, int w, int h, int c, IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_mat_get_dims(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_mat_get_w(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_mat_get_h(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_mat_get_c(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern ulong ncnn_mat_get_elemsize(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_mat_get_elempack(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern ulong ncnn_mat_get_cstep(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_get_data(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern float ncnn_mat_get_data_float(IntPtr mat, int index);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_mat_set_data_float(IntPtr mat, int index, float value);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_mat_substract_mean_normalize(IntPtr mat, float[] mean_vals, float[] norm_vals);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_convert_packing(IntPtr mat, out IntPtr dst, int elempack, IntPtr option);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_flatten(IntPtr src, out IntPtr dst, IntPtr option);

        #endregion

        #region MAT_PIXEL_API

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_from_pixels(
            IntPtr pixels,
            int type,
            int w,
            int h,
            int stride,
            IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_from_pixels_resize(
            IntPtr pixels,
            int type,
            int w,
            int h,
            int stride,
            int target_width,
            int target_height,
            IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_from_pixels_roi(
            IntPtr pixels,
            int type,
            int w,
            int h,
            int stride,
            int roix,
            int roiy,
            int roiw,
            int roih,
            IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_mat_from_pixels_roi_resize(
            IntPtr pixels,
            int type,
            int w,
            int h,
            int stride,
            int roix,
            int roiy,
            int roiw,
            int roih,
            int target_width,
            int target_height,
            IntPtr allocator);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_mat_to_pixels(IntPtr mat, IntPtr pixels, int type, int stride);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_mat_to_pixels_resize(
            IntPtr mat,
            IntPtr pixels,
            int type,
            int target_width,
            int target_height,
            int target_stride);

        #endregion

        #region BLOB_API

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_blob_get_name(IntPtr blob);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_blob_get_producer(IntPtr blob);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_blob_get_consumer(IntPtr blob);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_blob_get_shape(IntPtr blob, out int dims, out int w, out int h, out int c);

        #endregion

        #region PARAMDICT_API

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_paramdict_create();

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_paramdict_destroy(IntPtr pd);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_paramdict_get_type(IntPtr pd, int id);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_paramdict_get_int(IntPtr pd, int id, int def);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern float ncnn_paramdict_get_float(IntPtr pd, int id, float def);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_paramdict_get_array(IntPtr pd, int id, IntPtr def);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_paramdict_set_int(IntPtr pd, int id, int i);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_paramdict_set_float(IntPtr pd, int id, float f);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_paramdict_set_array(IntPtr pd, int id, IntPtr mat);

        #endregion

        #region DATAREADER_API

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_datareader_create();

        /*[DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_datareader_create_from_stdio(IntPtr fp);*/

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_datareader_create_from_memory(ref byte[] buffer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_datareader_destroy(IntPtr dt);

        #endregion

        #region MODELBIN_API

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_modelbin_create_from_datareader(IntPtr datareader);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_modelbin_create_from_mat_array(ref IntPtr weights, int n);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_modelbin_destroy(IntPtr mb);

        #endregion

        #region LAYER_API

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_layer_create_by_type(string type);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_layer_create_by_typeindex(int typeindex);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_layer_destroy(IntPtr layer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_layer_get_name(IntPtr layer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_get_typeindex(IntPtr layer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_layer_get_type(IntPtr layer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_get_one_blob_only(IntPtr layer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_get_support_inplace(IntPtr layer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_get_support_vulkan(IntPtr layer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_get_support_packing(IntPtr layer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_get_support_bf16_storage(IntPtr layer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_get_support_fp16_storage(IntPtr layer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_get_support_image_storage(IntPtr layer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_set_one_blob_only(IntPtr layer, int enable);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_set_support_inplace(IntPtr layer, int enable);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_set_support_vulkan(IntPtr layer, int enable);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_set_support_packing(IntPtr layer, int enable);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_set_support_bf16_storage(IntPtr layer, int enable);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_set_support_fp16_storage(IntPtr layer, int enable);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_set_support_image_storage(IntPtr layer, int enable);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_get_bottom_count(IntPtr layer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_get_bottom(IntPtr layer, int i);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_get_top_count(IntPtr layer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_layer_get_top(IntPtr layer, int i);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_blob_get_bottom_shape(IntPtr layer, int i, out int dims, out int w, out int h, out int c);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_blob_get_top_shape(IntPtr layer, int i, out int dims, out int w, out int h, out int c);

        #endregion

        #region NET_API

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_net_create();

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_net_destroy(IntPtr net);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_net_set_option(IntPtr net, IntPtr opt);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_net_register_custom_layer_by_type(
            IntPtr net,
            string type,
            IntPtr creator,
            IntPtr destroyer,
            IntPtr userdata);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_net_register_custom_layer_by_typeindex(
            IntPtr net,
            int typeindex,
            IntPtr creator,
            IntPtr destroyer,
            IntPtr userdata);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_net_load_param(IntPtr net, string path);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_net_load_param_bin(IntPtr net, string path);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_net_load_model(IntPtr net, string path);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_net_load_param_memory(IntPtr net, byte[] buffer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_net_load_param_bin_memory(IntPtr net, byte[] buffer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_net_load_model_memory(IntPtr net, byte[] buffer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_net_clear(IntPtr net);

        #endregion

        #region EXTRACTOR_API

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_extractor_create(IntPtr net);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_extractor_destroy(IntPtr ext);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_extractor_set_option(IntPtr ext, IntPtr opt);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_extractor_input(IntPtr ext, string name, IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_extractor_extract(IntPtr ext, string name, out IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_extractor_input_index(IntPtr ext, int index, IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_extractor_extract_index(IntPtr ext, int index, out IntPtr mat);

        #endregion

        #region SIMPLE_OCV_API

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_cv_imread(string path, int flags);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_cv_imread_mem(byte[] buffer, int len, int flags);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_cv_imwrite(IntPtr mat, string path);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_cv_imwrite_mem(IntPtr mat, out int len);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_cv_imwrite_mem_destroy(IntPtr buffer);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_cv_resize(IntPtr src, CvPoint size, float sw, float sh, int flags);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_cv_mat_clone(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern void ncnn_cv_mat_destroy(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_cv_mat_get_empty(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_cv_mat_get_channels(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_cv_mat_get_type(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern IntPtr ncnn_cv_mat_get_data(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_cv_mat_get_rows(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_cv_mat_get_cols(IntPtr mat);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_cv_mat_rectangle(
            IntPtr mat,
            CvRect rect,
            CvColor color,
            int thickness);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_cv_mat_circle(
            IntPtr mat,
            CvPoint center,
            int radius,
            CvColor color,
            int thickness);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_cv_mat_line(
            IntPtr mat,
            CvPoint pt1,
            CvPoint pt2,
            CvColor color,
            int thickness);

        [DllImport(LIB, CallingConvention = CONV)]
        public static extern int ncnn_cv_mat_text(
            IntPtr mat,
            string text,
            CvPoint pt,
            int fontFace,
            double fontScale,
            CvColor color,
            int thickness);

        #endregion
    }
}