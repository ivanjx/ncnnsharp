﻿$basedir = $pwd
$runtimes = Join-Path $basedir "\runtimes"

### Windows
# Create build folder.
mkdir ncnnbuild
cd ncnnbuild

# Install protobuf.
git clone https://github.com/protocolbuffers/protobuf.git --branch v3.17.2
cd protobuf 
git pull
mkdir winbuild
cd winbuild
cmake -G"NMake Makefiles" `
	  -DCMAKE_BUILD_TYPE=Release `
	  -DCMAKE_INSTALL_PREFIX=install `
	  -Dprotobuf_BUILD_TESTS=OFF `
	  -Dprotobuf_MSVC_STATIC_RUNTIME=OFF `
	  ../cmake
nmake
nmake install
$protobuild = Join-Path $pwd "\install"
$protoinclude = Join-Path $protobuild "\include"
$protolib = Join-Path $protobuild "\lib\libprotobuf.lib"
$protoc = Join-Path $protobuild "\bin\protoc.exe"
cd $basedir

# Install ncnn.
cd ncnnbuild
git clone --depth=1 https://github.com/realivanjx/ncnn.git --branch simpleocv-c
cd ncnn
git pull
mkdir winbuild
cd winbuild
cmake -G"NMake Makefiles" `
	  -D CMAKE_BUILD_TYPE=Release `
	  -D CMAKE_INSTALL_PREFIX=install `
	  -D Protobuf_INCLUDE_DIR=$protoinclude `
	  -D Protobuf_LIBRARIES=$protolib `
	  -D Protobuf_PROTOC_EXECUTABLE=$protoc `
	  -D NCNN_SHARED_LIB=ON `
	  -D NCNN_ENABLE_LTO=ON `
	  -D NCNN_SIMPLEOCV=ON `
	  -D NCNN_BUILD_EXAMPLES=OFF `
	  -D NCNN_BUILD_TOOLS=OFF `
	  ..
nmake
nmake install

# Copy result.
$winruntime = Join-Path $runtimes "\win-x86\native"
rm -r -fo $winruntime
mkdir $winruntime
cp install\bin\ncnn.dll $winruntime
cd $basedir

# x64 build.
# https://github.com/takuya-takeuchi/NcnnDotNet/blob/master/nuget/BuildUtils.ps1#L407
function CallVisualStudioDeveloperConsole()
{
   cmd.exe /c "call `"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat`" && set > %temp%\vcvars.txt"

   Get-Content "${env:temp}\vcvars.txt" | Foreach-Object {
      if ($_ -match "^(.*?)=(.*)$") {
        Set-Content "env:\$($matches[1])" $matches[2]
      }
    }
}

cd ncnnbuild\ncnn
mkdir winbuild64
cd winbuild64
CallVisualStudioDeveloperConsole
cmake -G"NMake Makefiles" `
	  -D CMAKE_BUILD_TYPE=Release `
	  -D CMAKE_INSTALL_PREFIX=install `
	  -D Protobuf_INCLUDE_DIR=$protoinclude `
	  -D Protobuf_LIBRARIES=$protolib `
	  -D Protobuf_PROTOC_EXECUTABLE=$protoc `
	  -D NCNN_SHARED_LIB=ON `
	  -D NCNN_ENABLE_LTO=ON `
	  -D NCNN_SIMPLEOCV=ON `
	  -D NCNN_BUILD_EXAMPLES=OFF `
	  -D NCNN_BUILD_TOOLS=OFF `
	  ..
nmake
nmake install

# Copy result.
$winruntime = Join-Path $runtimes "\win-x64\native"
rm -r -fo $winruntime
mkdir $winruntime
cp install\bin\ncnn.dll $winruntime
cd $basedir


### Linux x64
#docker build -t ncnn-x64 ncnn\x64
#docker run -it --rm -v $pwd\ncnn\result:/result ncnn-x64


### Linux arm64
#docker build -t ncnn-arm64 ncnn\arm64
#docker run -it --rm -v $pwd\ncnn\result:/result ncnn-arm64