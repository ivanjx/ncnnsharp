﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ncnnsharp
{
    public class Extractor : IDisposable
    {
        IntPtr m_handle;

        public Extractor(IntPtr handle)
        {
            m_handle = handle;
        }

        public void SetOption(Option opt)
        {
            Native.ncnn_extractor_set_option(m_handle, opt.Handle);
        }

        public void Input(string name, Mat mat)
        {
            Native.ncnn_extractor_input(m_handle, name, mat.Handle);
        }

        public void Input(int index, Mat mat)
        {
            Native.ncnn_extractor_input_index(m_handle, index, mat.Handle);
        }

        public Mat Extract(string name)
        {
            IntPtr ptr;
            Native.ncnn_extractor_extract(m_handle, name, out ptr);
            return new Mat(ptr);
        }

        public Mat Extract(int index)
        {
            IntPtr ptr;
            Native.ncnn_extractor_extract_index(m_handle, index, out ptr);
            return new Mat(ptr);
        }

        public void Dispose()
        {
            Native.ncnn_extractor_destroy(m_handle);
        }
    }
}
