﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ncnnsharp
{
    public class Allocator : IDisposable
    {
        IntPtr m_handle;

        public IntPtr Handle
        {
            get
            {
                return m_handle;
            }
        }

        public Allocator(bool unlocked = false)
        {
            if (unlocked)
            {
                m_handle = Native.ncnn_allocator_create_unlocked_pool_allocator();
            }
            else
            {
                m_handle = Native.ncnn_allocator_create_pool_allocator();
            }
        }

        public Allocator(IntPtr handle)
        {
            m_handle = handle;
        }

        public void Dispose()
        {
            Native.ncnn_allocator_destroy(m_handle);
        }
    }
}
