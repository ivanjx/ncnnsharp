﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ncnnsharp
{
    public class ParamDict : IDisposable
    {
        IntPtr m_handle;

        public IntPtr Handle
        {
            get
            {
                return m_handle;
            }
        }

        public ParamDict()
        {
            m_handle = Native.ncnn_paramdict_create();
        }

        public ParamDict(IntPtr handle)
        {
            m_handle = handle;
        }

        public void Set(int id, int i)
        {
            Native.ncnn_paramdict_set_int(m_handle, id, i);
        }

        public void Set(int id, float f)
        {
            Native.ncnn_paramdict_set_float(m_handle, id, f);
        }

        public void Set(int id, Mat m)
        {
            Native.ncnn_paramdict_set_array(m_handle, id, m.Handle);
        }

        public int GetInt(int id)
        {
            return Native.ncnn_paramdict_get_int(m_handle, id, 0);
        }

        public float GetFloat(int id)
        {
            return Native.ncnn_paramdict_get_float(m_handle, id, 0f);
        }

        public Mat GetArray(int id)
        {
            return new Mat(
                Native.ncnn_paramdict_get_array(m_handle, id, IntPtr.Zero));
        }

        public void Dispose()
        {
            Native.ncnn_paramdict_destroy(m_handle);
        }
    }
}
