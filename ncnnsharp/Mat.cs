﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ncnnsharp
{
    public class Mat : IDisposable
    {
        IntPtr m_handle;
        Allocator m_allocator;

        public IntPtr Handle
        {
            get
            {
                return m_handle;
            }
        }

        public int Dims
        {
            get
            {
                return Native.ncnn_mat_get_dims(m_handle);
            }
        }

        public int C
        {
            get
            {
                return Native.ncnn_mat_get_c(m_handle);
            }
        }

        public int W
        {
            get
            {
                return Native.ncnn_mat_get_w(m_handle);
            }
        }

        public int H
        {
            get
            {
                return Native.ncnn_mat_get_h(m_handle);
            }
        }

        public ulong ElemSize
        {
            get
            {
                return Native.ncnn_mat_get_elemsize(m_handle);
            }
        }

        public int ElemPack
        {
            get
            {
                return Native.ncnn_mat_get_elempack(m_handle);
            }
        }

        public ulong CStep
        {
            get
            {
                return Native.ncnn_mat_get_cstep(m_handle);
            }
        }

        public IntPtr Data
        {
            get
            {
                return Native.ncnn_mat_get_data(m_handle);
            }
        }

        public float this[int index]
        {
            get
            {
                return Native.ncnn_mat_get_data_float(m_handle, index);
            }
            set
            {
                Native.ncnn_mat_set_data_float(m_handle, index, value);
            }
        }

        public float[] GetRow(int index)
        {
            float[] result = new float[W];

            for (int i = 0; i < W; ++i)
            {
                result[i] = this[i + (index * W)];
            }

            return result;
        }

        public Mat(IntPtr handle, Allocator allocator = null)
        {
            m_handle = handle;
            m_allocator = allocator;
        }

        public Mat(int w, Allocator allocator = null)
        {
            m_handle = Native.ncnn_mat_create_1d(
                w, 
                allocator?.Handle ?? IntPtr.Zero);
            m_allocator = allocator;
        }

        public Mat(int w, int h, Allocator allocator = null)
        {
            m_handle = Native.ncnn_mat_create_2d(
                w,
                h,
                allocator?.Handle ?? IntPtr.Zero);
            m_allocator = allocator;
        }

        public Mat(int w, int h, int c, Allocator allocator = null)
        {
            m_handle = Native.ncnn_mat_create_3d(
                w,
                h,
                c,
                allocator?.Handle ?? IntPtr.Zero);
            m_allocator = allocator;
        }

        public Mat(int w, IntPtr data, Allocator allocator = null)
        {
            m_handle = Native.ncnn_mat_create_external_1d(
                w,
                data,
                allocator?.Handle ?? IntPtr.Zero);
            m_allocator = allocator;
        }

        public Mat(int w, int h, IntPtr data, Allocator allocator = null)
        {
            m_handle = Native.ncnn_mat_create_external_2d(
                w,
                h,
                data,
                allocator?.Handle ?? IntPtr.Zero);
            m_allocator = allocator;
        }

        public Mat(int w, int h, int c, IntPtr data, Allocator allocator = null)
        {
            m_handle = Native.ncnn_mat_create_external_3d(
                w,
                h,
                c,
                data,
                allocator?.Handle ?? IntPtr.Zero);
            m_allocator = allocator;
        }

        public Mat(int w, ulong elemsize, int elempack, Allocator allocator = null)
        {
            m_handle = Native.ncnn_mat_create_1d_elem(
                w,
                elemsize,
                elempack,
                allocator?.Handle ?? IntPtr.Zero);
            m_allocator = allocator;
        }

        public Mat(int w, int h, ulong elemsize, int elempack, Allocator allocator = null)
        {
            m_handle = Native.ncnn_mat_create_2d_elem(
                w,
                h,
                elemsize,
                elempack,
                allocator?.Handle ?? IntPtr.Zero);
            m_allocator = allocator;
        }

        public Mat(int w, int h, int c, ulong elemsize, int elempack, Allocator allocator = null)
        {
            m_handle = Native.ncnn_mat_create_3d_elem(
                w,
                h,
                c,
                elemsize,
                elempack,
                allocator?.Handle ?? IntPtr.Zero);
            m_allocator = allocator;
        }

        public Mat(int w, IntPtr data, ulong elemsize, int elempack, Allocator allocator = null)
        {
            m_handle = Native.ncnn_mat_create_external_1d_elem(
                w,
                data,
                elemsize,
                elempack,
                allocator?.Handle ?? IntPtr.Zero);
            m_allocator = allocator;
        }

        public Mat(int w, int h, IntPtr data, ulong elemsize, int elempack, Allocator allocator = null)
        {
            m_handle = Native.ncnn_mat_create_external_2d_elem(
                w,
                h,
                data,
                elemsize,
                elempack,
                allocator?.Handle ?? IntPtr.Zero);
            m_allocator = allocator;
        }

        public Mat(int w, int h, int c, IntPtr data, ulong elemsize, int elempack, Allocator allocator = null)
        {
            m_handle = Native.ncnn_mat_create_external_3d_elem(
                w,
                h,
                c,
                data,
                elemsize,
                elempack,
                allocator?.Handle ?? IntPtr.Zero);
            m_allocator = allocator;
        }

        public void FillFloat(float v)
        {
            Native.ncnn_mat_fill_float(m_handle, v);
        }

        public void SubstractMeanNormalize(float[] meanVals, float[] normVals = null)
        {
            if (normVals == null)
            {
                normVals = new[] { 0f };
            }

            Native.ncnn_mat_substract_mean_normalize(m_handle, meanVals, normVals);
        }

        public Mat Clone()
        {
            return new Mat(Native.ncnn_mat_clone(
                m_handle,
                m_allocator?.Handle ?? IntPtr.Zero));
        }

        public Mat Reshape(int w)
        {
            return new Mat(Native.ncnn_mat_reshape_1d(
                m_handle,
                w,
                m_allocator?.Handle ?? IntPtr.Zero));
        }

        public Mat Reshape(int w, int h)
        {
            return new Mat(Native.ncnn_mat_reshape_2d(
                m_handle,
                w,
                h,
                m_allocator?.Handle ?? IntPtr.Zero));
        }

        public Mat Reshape(int w, int h, int c)
        {
            return new Mat(Native.ncnn_mat_reshape_3d(
                m_handle,
                w,
                h,
                c,
                m_allocator?.Handle ?? IntPtr.Zero));
        }

        public Mat ConvertPacking(int elemPack, Option option = null)
        {
            if (option == null)
            {
                option = new Option();
            }

            IntPtr res;
            Native.ncnn_convert_packing(
                m_handle,
                out res,
                elemPack,
                option.Handle);
            return new Mat(res, m_allocator);
        }

        public Mat Flatten(Option option = null)
        {
            if (option == null)
            {
                option = new Option();
            }

            IntPtr res;
            Native.ncnn_flatten(
                m_handle,
                out res,
                option.Handle);
            return new Mat(res, m_allocator);
        }

        public void Dispose()
        {
            Native.ncnn_mat_destroy(m_handle);
        }

        #region PIXEL_API

        public static Mat FromPixels(
            IntPtr pixels, 
            int type, 
            int w, 
            int h, 
            int stride, 
            Allocator allocator = null)
        {
            return new Mat(Native.ncnn_mat_from_pixels(
                pixels,
                type,
                w,
                h,
                stride,
                allocator?.Handle ?? IntPtr.Zero));
        }

        public static Mat FromPixelsResize(
            IntPtr pixels,
            int type,
            int w,
            int h,
            int stride,
            int targetWidth,
            int targetHeight,
            Allocator allocator = null)
        {
            return new Mat(Native.ncnn_mat_from_pixels_resize(
                pixels,
                type,
                w,
                h,
                stride,
                targetWidth,
                targetHeight,
                allocator?.Handle ?? IntPtr.Zero));
        }

        public static Mat FromPixelsResize(
            IntPtr pixels,
            int type,
            int w,
            int h,
            int targetWidth,
            int targetHeight,
            Allocator allocator = null)

        {
            int typefrom = type & 65535;

            if (typefrom == 1 || typefrom == 2)
            {
                return FromPixelsResize(
                    pixels,
                    type,
                    w,
                    h,
                    w * 3,
                    targetWidth,
                    targetHeight,
                    allocator);
            }
            else if (typefrom == 3)
            {
                return FromPixelsResize(
                    pixels,
                    type,
                    w,
                    h,
                    w,
                    targetWidth,
                    targetHeight,
                    allocator);
            }

            else if (typefrom == 4 || typefrom == 5)
            {

                return FromPixelsResize(
                    pixels,
                    type,
                    w,
                    h,
                    w * 4,
                    targetWidth,
                    targetHeight,
                    allocator);
            }

            throw new ArgumentException("Unknown type");
        }

        public static Mat FromPixelsROI(
            IntPtr pixels,
            int type,
            int w,
            int h,
            int stride,
            int roix,
            int roiy,
            int roiw,
            int roih,
            Allocator allocator = null)
        {
            return new Mat(Native.ncnn_mat_from_pixels_roi(
                pixels,
                type,
                w,
                h,
                stride,
                roix,
                roiy,
                roiw,
                roih,
                allocator?.Handle ?? IntPtr.Zero));
        }

        public static Mat FromPixelsROIResize(
            IntPtr pixels,
            int type,
            int w,
            int h,
            int stride,
            int roix,
            int roiy,
            int roiw,
            int roih,
            int targetWidth,
            int targetHeight,
            Allocator allocator = null)
        {
            return new Mat(Native.ncnn_mat_from_pixels_roi_resize(
                pixels,
                type,
                w,
                h,
                stride,
                roix,
                roiy,
                roiw,
                roih,
                targetWidth,
                targetHeight,
                allocator?.Handle ?? IntPtr.Zero));
        }

        #endregion
    }
}
