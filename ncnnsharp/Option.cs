using System;

namespace ncnnsharp
{
    public class Option : IDisposable
    {
        IntPtr m_handle;

        public IntPtr Handle
        {
            get
            {
                return m_handle;
            }
        }

        public int NumThreads
        {
            get
            {
                return Native.ncnn_option_get_num_threads(m_handle);
            }
            set
            {
                Native.ncnn_option_set_num_threads(m_handle, value);
            }
        }

        public bool UseVulkanCompute
        {
            get
            {
                int val = Native.ncnn_option_get_use_vulkan_compute(m_handle);
                return val != 0;
            }
            set
            {
                Native.ncnn_option_set_use_vulkan_compute(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool UseWinogradConvolution
        {
            get
            {
                int val = Native.ncnn_option_get_use_winograd_convolution(m_handle);
                return val != 0;
            }
            set
            {
                Native.ncnn_option_set_use_winograd_convolution(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool UseSgemmConvolution
        {
            get
            {
                int val = Native.ncnn_option_get_use_sgemm_convolution(m_handle);
                return val != 0;
            }
            set
            {
                Native.ncnn_option_set_use_sgemm_convolution(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool UseFP16Storage
        {
            get
            {
                int val = Native.ncnn_option_get_use_fp16_storage(m_handle);
                return val != 0;
            }
            set
            {
                Native.ncnn_option_set_use_fp16_storage(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool UseFP16Packed
        {
            get
            {
                int val = Native.ncnn_option_get_use_fp16_packed(m_handle);
                return val != 0;
            }
            set
            {
                Native.ncnn_option_set_use_fp16_packed(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool UseFP16Arithmetic
        {
            get
            {
                int val = Native.ncnn_option_get_use_fp16_arithmetic(m_handle);
                return val != 0;
            }
            set
            {
                Native.ncnn_option_set_use_fp16_arithmetic(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool UsePackingLayout
        {
            get
            {
                int val = Native.ncnn_option_get_use_packing_layout(m_handle);
                return val != 0;
            }
            set
            {
                Native.ncnn_option_set_use_packing_layout(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool UseShaderPack8
        {
            get
            {
                int val = Native.ncnn_option_get_use_shader_pack8(m_handle);
                return val != 0;
            }
            set
            {
                Native.ncnn_option_set_use_shader_pack8(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool UseImageStorage
        {
            get
            {
                int val = Native.ncnn_option_get_use_image_storage(m_handle);
                return val != 0;
            }
            set
            {
                Native.ncnn_option_set_use_image_storage(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public Option()
        {
            m_handle = Native.ncnn_option_create();
        }

        public void Dispose()
        {
            Native.ncnn_option_destroy(m_handle);
        }
    }
}