﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ncnnsharp
{
    public class Layer : IDisposable
    {
        IntPtr m_handle;

        public IntPtr Handle
        {
            get
            {
                return m_handle;
            }
        }

        public string Name
        {
            get
            {
                IntPtr ptr = Native.ncnn_layer_get_name(m_handle);
                return Marshal.PtrToStringAnsi(ptr);
            }
        }

        public int TypeIndex
        {
            get
            {
                return Native.ncnn_layer_get_typeindex(m_handle);
            }
        }

        public string Type
        {
            get
            {
                IntPtr ptr = Native.ncnn_layer_get_type(m_handle);
                return Marshal.PtrToStringAnsi(ptr);
            }
        }

        public bool IsOneBlobOnly
        {
            get
            {
                return Native.ncnn_layer_get_one_blob_only(m_handle) != 0;
            }
            set
            {
                Native.ncnn_layer_set_one_blob_only(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool IsSupportInPlace
        {
            get
            {
                return Native.ncnn_layer_get_support_inplace(m_handle) != 0;
            }
            set
            {
                Native.ncnn_layer_set_support_inplace(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool IsSupportVulkan
        {
            get
            {
                return Native.ncnn_layer_get_support_vulkan(m_handle) != 0;
            }
            set
            {
                Native.ncnn_layer_set_support_vulkan(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool IsSupportPacking
        {
            get
            {
                return Native.ncnn_layer_get_support_packing(m_handle) != 0;
            }
            set
            {
                Native.ncnn_layer_set_support_packing(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool IsSupportBF16Storage
        {
            get
            {
                return Native.ncnn_layer_get_support_bf16_storage(m_handle) != 0;
            }
            set
            {
                Native.ncnn_layer_set_support_bf16_storage(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool IsSupportFP16Storage
        {
            get
            {
                return Native.ncnn_layer_get_support_fp16_storage(m_handle) != 0;
            }
            set
            {
                Native.ncnn_layer_set_support_fp16_storage(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public bool IsSupportImageStorage
        {
            get
            {
                return Native.ncnn_layer_get_support_image_storage(m_handle) != 0;
            }
            set
            {
                Native.ncnn_layer_set_support_image_storage(
                    m_handle,
                    value ? 1 : 0);
            }
        }

        public int BottomCount
        {
            get
            {
                return Native.ncnn_layer_get_bottom_count(m_handle);
            }
        }

        public int TopCount
        {
            get
            {
                return Native.ncnn_layer_get_top_count(m_handle);
            }
        }

        public Layer(IntPtr handle)
        {
            m_handle = handle;
        }

        public int GetBottom(int i)
        {
            return Native.ncnn_layer_get_bottom(m_handle, i);
        }

        public void GetBottomShape(int i, out int dims, out int w, out int h, out int c)
        {
            Native.ncnn_blob_get_bottom_shape(
                m_handle,
                i,
                out dims,
                out w,
                out h,
                out c);
        }

        public int GetTop(int i)
        {
            return Native.ncnn_layer_get_top(m_handle, i);
        }

        public void GetTopShape(int i, out int dims, out int w, out int h, out int c)
        {
            Native.ncnn_blob_get_top_shape(
                m_handle,
                i,
                out dims,
                out w,
                out h,
                out c);
        }

        public void Dispose()
        {
            Native.ncnn_layer_destroy(m_handle);
        }

        public static Layer Create(string type)
        {
            return new Layer(
                Native.ncnn_layer_create_by_type(type));
        }

        public static Layer Create(int typeIndex)
        {
            return new Layer(
                Native.ncnn_layer_create_by_typeindex(typeIndex));
        }
    }
}
